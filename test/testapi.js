var mocha = require('mocha');
var chai = require('chai');
var chaihttp = require('chai-http');

var server = require('../server.js');
var user = require('../usu_loginpass.json');

chai.use(chaihttp);

var should = chai.should();

describe('First test',
 function() {
   it('Tests that DuckDuckGo works', function(done) {
     chai.request('http://www.duckduckgo.com')
       .get('/')
       .end(
         function(err, res) {
           console.log("Request has ended");
           // console.log(res);
           console.log(err);
           done();
         }
       );
   });
 }
);

describe('Test de API Usuarios',
 function() {
   it('Prueba que la API de usuarios responde correctamente.',
     function(done) {
       chai.request('http://localhost:3000')
         .get('/apitechu/v1')
         .end(
           function(err, res) {
             res.should.have.status(200);
             res.body.msg.should.be.eql("güelcome chu de API of Tech University Molon")
             done();
           }
         )
     }
   )
 }
);
it('Prueba que la API devuelve una lista de usuarios correctos.',
     function(done) {
       chai.request('http://localhost:3000')
       .get('/apitechu/v1/users')
       .end(
         function(err, res) {
           res.should.have.status(200);
           res.body.should.be.a("array");
           for (user of res.body) {
             user.should.have.property('email');
             //user.should.have.property('country') //Esta línea fallara ya que country es un campo que no existe en el fichero
             user.should.have.property('password');
           }
           done();
         }
       )
     }
   )
