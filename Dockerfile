# Imagen raíz
FROM node

# Carpeta raíz
WORKDIR /apitechu

# copia de archivos
ADD . /apitechu

#Exponer puerto
EXPOSE 3000

#COMANDO DE inicializacion
CMD ["npm", "start"]
